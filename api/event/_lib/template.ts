import { readFileSync } from "fs";
import marked from "marked";
import { sanitizeHtml } from "../../_lib/sanitizer";
import { ParsedRequest } from "./types";
const twemoji = require("twemoji");
const twOptions = { folder: "svg", ext: ".svg" };
const emojify = (text: string) => twemoji.parse(text, twOptions);
console.log(__dirname);
const fontDirectory = "/../..";
const rglr = readFileSync(
  `${__dirname}${fontDirectory}/_fonts/Inter-Regular.woff2`
).toString("base64");
const bold = readFileSync(
  `${__dirname}${fontDirectory}//_fonts/Inter-Bold.woff2`
).toString("base64");
const mono = readFileSync(
  `${__dirname}${fontDirectory}//_fonts/Vera-Mono.woff2`
).toString("base64");

function getCss() {
  let background = "white";
  let foreground = "black";
  let radial = "lightgray";
  return `
  @font-face {
      font-family: 'Inter';
      font-style:  normal;
      font-weight: normal;
      src: url(data:font/woff2;charset=utf-8;base64,${rglr}) format('woff2');
  }

  @font-face {
      font-family: 'Inter';
      font-style:  normal;
      font-weight: bold;
      src: url(data:font/woff2;charset=utf-8;base64,${bold}) format('woff2');
  }

  @font-face {
      font-family: 'Vera';
      font-style: normal;
      font-weight: normal;
      src: url(data:font/woff2;charset=utf-8;base64,${mono})  format("woff2");
    }

  body {
      background: ${background};
      background-image: radial-gradient(circle at 25px 25px, ${radial} 2%, transparent 0%), radial-gradient(circle at 75px 75px, ${radial} 2%, transparent 0%);
      background-size: 100px 100px;
      height: 100vh;
      display: flex;
      text-align: center;
      align-items: center;
      justify-content: center;
  }

  code {
      color: #D400FF;
      font-family: 'Vera';
      white-space: pre-wrap;
      letter-spacing: -5px;
  }

  code:before, code:after {
      content: '\`';
  }

  .logo-wrapper {
      display: flex;
      align-items: center;
      align-content: center;
      justify-content: center;
      justify-items: center;
  }

  .logo {
      margin: 0 75px;
      border: 2em solid white;
      border-radius: 50%;
  }

  .plus {
      color: #BBB;
      font-family: Times New Roman, Verdana;
      font-size: 100px;
  }

  .spacer {
      margin: 0 100px;
  }

  .emoji {
      height: 1em;
      width: 1em;
      margin: 0 .05em 0 .1em;
      vertical-align: -0.1em;
  }
  
  .heading {
      font-family: 'Inter', sans-serif;
      font-size: 96px;
      font-style: normal;
      font-weight: bold;
      color: ${foreground};
      line-height: 1.8;
  }
  .heading p {
    margin:0;
  }
  
  .subheading {
      font-family: 'Inter', sans-serif;
      font-size: 65px;
      font-style: normal;
      font-weight: normal;
      color: ${foreground};
      line-height: 1.8;
  }
  
  .subheading p {
    margin:0;
  }`;
}

export function getHtml(parsedReq: ParsedRequest) {
  const { title, subtitle, md } = parsedReq;
  return `<!DOCTYPE html>
<html>
    <meta charset="utf-8">
    <title>Generated Image</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        ${getCss()}
    </style>
    <body>
    <div>
        <div class="spacer">
        <div class="heading">${emojify(
          md ? marked(title) : sanitizeHtml(title)
        )}</div>
        <div class="subheading">${emojify(
          md ? marked(subtitle) : sanitizeHtml(subtitle)
        )}</div>
    </div>
    </body>
</html>`;
}
