import { IncomingMessage } from "http";
import { parse } from "url";
import { ParsedRequest } from "./types";
import { Theme } from "../../_lib/chromium";

export function parseRequest(req: IncomingMessage) {
  const { pathname, query } = parse(req.url || "/", true);
  const { fontSize, images, widths, heights, htmlDebug } = query || {};

  if (Array.isArray(fontSize)) {
    throw new Error("Expected a single fontSize");
  }

  const arr = (pathname || "/").slice(1).split(".");
  let extension = "";
  let text = "";
  console.log(arr);
  if (arr.length === 0) {
    text = "";
  } else if (arr.length === 1) {
    text = arr[0];
  } else {
    extension = arr.pop() as string;
    text = arr.join(".");
  }
  text = text.substring(8);

  const parsedRequest: ParsedRequest = {
    htmlDebug: htmlDebug === "1" || htmlDebug === "true",
    fileType: extension === "jpeg" ? extension : "png",
    text: decodeURIComponent(text),
    theme: "light",
    md: true,
    fontSize: fontSize || "96px",
    images: getArray(images),
    widths: getArray(widths),
    heights: getArray(heights),
  };
  parsedRequest.images = getDefaultImages(
    parsedRequest.images,
    parsedRequest.theme
  );
  return parsedRequest;
}

function getArray(stringOrArray: string[] | string | undefined): string[] {
  if (typeof stringOrArray === "undefined") {
    return [];
  } else if (Array.isArray(stringOrArray)) {
    return stringOrArray;
  } else {
    return [stringOrArray];
  }
}

function getDefaultImages(images: string[], theme: Theme): string[] {
  const defaultImage =
    theme === "light"
      ? "https://ik.imagekit.io/micalendario/site/icon-border_P-JoXosKsLp.png"
      : "https://ik.imagekit.io/micalendario/site/icon-border_P-JoXosKsLp.png";

  if (!images || !images[0]) {
    return [defaultImage];
  }
  if (!images[0].startsWith("https://ik.imagekit.io/micalendario/")) {
    images[0] = defaultImage;
  }
  return images;
}
