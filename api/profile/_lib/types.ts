import { Theme, FileType } from "../../_lib/chromium";
export interface ParsedRequest {
  htmlDebug: boolean;
  fileType: FileType;
  text: string;
  theme: Theme;
  md: boolean;
  fontSize: string;
  images: string[];
  widths: string[];
  heights: string[];
}
